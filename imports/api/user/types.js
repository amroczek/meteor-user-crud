export const Types = `
  type User {
    email: String!
    _id: String
  }
  
  input UserEdit {
    email: String
  }
  
  type Inserted {
    email: String
    inserted: Int
  }
  
  type Removed {
    user: User
    removed: Int
  }
  
  type Edited {
    oldUser: User
    newUser: User
    success: Int!
  }
  
  type Query {
    ReadUsers: [User]
  }
  
  type Mutation {
    CreateUser(email: String!): Inserted
    DeleteUser(id: String!): Removed
    UpdateUser(id: String!, user: UserEdit): Edited
  }
`;
