import { Mongo } from 'meteor/mongo';

const Users = new Mongo.Collection('app-users');

const Read = (root, args, context) => {
  const users = Users.find({}).fetch();
  return users;
};

const Create = (root, args, context) => {
  
  const exists = Users.find({ email: args.email }).fetch();
  
  if (exists.length !== 0) {
    return {
      email: args.email,
      inserted: 0
    };
  }
  
  const user = {
    email: args.email
  };
  
  Users.insert(user);
  
  return {
    email: args.email,
    inserted: 1
  };
  
};

const Delete = (root, args, context) => {
  
  const User = Users.find({ _id: args.id }).fetch();
  
  if (User.length !== 0) {
    Users.remove({ _id: args.id });
    User[0].removed = 1;
    return User[0];
  }
  
  return {
    removed: 0,
    user: {}
  }
  
};

const Update = (root, args, context) => {
  
  const oldUser = Users.find({ _id: args.id }).fetch();
  
  if (oldUser.length !== 1) {
    return {
      oldUser: {},
      newUser: {},
      success: 0
    }
  }
  
  Users.update({ _id: args.id }, { $set: args.user });
  
  const newUser = Users.find({ _id: args.id }).fetch();
  
  return {
    oldUser: oldUser[0],
    newUser: newUser[0],
    success: 1
  }
};

export const User = {
  Create,
  Read,
  Update,
  Delete
};
