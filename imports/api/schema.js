import { User } from './user/user';
import { Types } from './user/types';

export const typeDefs = [
  Types
];

export const resolvers = {
  Query: {
    ReadUsers: User.Read,
  },
  Mutation: {
    CreateUser: User.Create,
    DeleteUser: User.Delete,
    UpdateUser: User.Update
  },
};
