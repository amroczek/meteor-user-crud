import React from 'react';
import PropTypes from 'prop-types';

import { RenderUsers } from './User/RenderUsers';
import { CreateUser } from './User/Create';

const App = () => (
  <div className="App">
    <CreateUser />
    <RenderUsers />
  </div>
);

App.propTypes = {
  currentUser: PropTypes.object,
  hasErrors: PropTypes.bool,
  refetch: PropTypes.func,
  userLoading: PropTypes.bool,
};

export default App;
