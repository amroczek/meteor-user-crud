import React from 'react';
import { gql, graphql } from 'react-apollo';

import { GET_USERS } from './RenderUsers';

const UpdateUserComponent = ({ mutate, user }) => {
  
  const id = user._id;

  const UpdateUserHandler = (evt) => {

    evt.persist();

    const email = window.prompt(`Enter new email for user ${user.email}`);

    if (!email) {
      return alert('Invalid data');
    }

    mutate({
      variables: { id, user: { email } },
      refetchQueries: [{ query: GET_USERS }]
    });

  }

  return <span className="edit" onClick={UpdateUserHandler}>Edit</span>

}

export const UpdateUserMutation = gql`
  mutation ($id: String!, $user: UserEdit!){
    UpdateUser(id: $id, user: $user) {
      success
    }
  }
`;

export const UpdateUser = graphql(
  UpdateUserMutation
)(UpdateUserComponent);