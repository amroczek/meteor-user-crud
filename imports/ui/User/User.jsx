import React, { Component } from 'react';

import { SingleUser } from './SingleUser';

export class User extends Component {

  render() {

    if (this.props.users.loading) {
      return <div>Loading...</div>;
    }

    return (

      <div>
        {this.props.users.ReadUsers.map((user )=> {
          return <SingleUser key={user._id} user={user} />;
        })}
      </div>
    );

  }

}
