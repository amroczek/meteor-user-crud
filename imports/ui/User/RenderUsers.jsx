import React from 'react';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

import { User } from './User';

function RenderComponent({ data }) {
  return <User users={data} />;
}

export const GET_USERS = gql`
{
  ReadUsers {
    _id
    email
  }
}`;

export const RenderUsers = graphql(GET_USERS)(RenderComponent);
