import React from 'react';
import { gql, graphql } from 'react-apollo';

import { GET_USERS } from './RenderUsers';

const CreateUserComponent = ({ mutate }) => {

  const CreateUserHandler = (evt) => {

    evt.preventDefault();
    evt.persist();

    const createUser = document.getElementById('create-user');
    const email = createUser.value;

    mutate({ 
      variables: { email },
      refetchQueries: [{ query: GET_USERS }]
    }).then(() => {
      createUser.value = '';
    });

  };

  return (
    <form>
      <label htmlFor="create-user">
        Add email
      </label>
      <br />
      <input id="create-user" />
      <button onClick={CreateUserHandler}>Add user</button>
    </form>
  );

};

export const CreateUserMutation = gql`
  mutation ($email: String!){
    CreateUser(email: $email) {
      inserted
    }
  }
`;

export const CreateUser = graphql(
  CreateUserMutation
)(CreateUserComponent);
