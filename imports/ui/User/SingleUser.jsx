import React from 'react';

import { UpdateUser } from './Update';
import { DeleteUser } from './Delete';

export const SingleUser = ({ user }) => {

  return (
    <div>
      {user.email} 
      <DeleteUser user={user} />
      <UpdateUser user={user} />
    </div>
  );

}