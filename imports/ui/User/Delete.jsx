import React from 'react';
import { gql, graphql } from 'react-apollo';

import { GET_USERS } from './RenderUsers';

const DeleteUserComponent = ({ mutate, user }) => {
  
  const id = user._id;

  const DeleteUserHandler = (evt) => {
    evt.persist();

    const confirm = window.confirm(`Are you sure, that you want to delete ${user.email}?`);

    if (!confirm) {
      return alert('Aborted');
    }

    mutate({
      variables: { id },
      refetchQueries: [{ query: GET_USERS }]
    });

  }

  return <span className="delete" onClick={DeleteUserHandler}>x</span>

}


export const DeleteUserMutation = gql`
mutation ($id: String!){
  DeleteUser(id: $id) {
    removed
  }
}
`;

export const DeleteUser = graphql(
    DeleteUserMutation
)(DeleteUserComponent);
